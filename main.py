import os
import shutil
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import PIL.ImageOps 

def get_data():
	data_list=[]
	for root,dirs,files in os.walk('train/'):
		for f in files:
			if f != '.DS_Store':
				p = os.path.join(root,f)
				data_list.append(p)
	# f is IMG_3315.jpg
	# p is ./2007/IMG_3315.jpg
	# root is ./2007
	# dir is ['2007','2008','2009']
	print(data_list)
	return data_list

def img_to_array(img_path):
	l = len(img_path)
	for i in range(l):
		img = Image.open(img_path[i])
		img = np.array(img)
		img_list[i] = img
	return img_list

def rename_data(data,start,end):
	# year = data[]
	dirpath = 'output/'
	if not os.path.exists(dirpath):
		os.mkdir(dirpath)
	new_data = dirpath+'/'+start+'-'+end+'.jpg'
	new_path = shutil.move(data,new_data)


def main():
	get_data()
	# rename_data('./2007/IMG_3315.JPG','0303','0326')
	


if __name__ == '__main__':
	main()